import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from './admin.component';
import {DashboardComponent} from './views/dashboard/dashboard.component';
import {BrandsComponent} from './views/brands/brands.component';
import {AdminGuard} from '@product/guards/admin.guard';
import {CategoriesComponent} from './views/categories/categories.component';
import {ProductsComponent} from './views/products/products.component';
import {ProductsDetailComponent} from './views/products/products-detail/products-detail.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [ AdminGuard ],
    children: [
      { path: '', component: DashboardComponent },
      { path: 'marcas', component: BrandsComponent },
      { path: 'categorias', component: CategoriesComponent },
      { path: 'productos', component: ProductsComponent },
      { path: 'productos/:id', component: ProductsDetailComponent }
    ]
  },
  {
    path: 'login',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
