import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { SignInComponent } from './views/sign-in/sign-in.component';
import {AuthMaterialModule} from './auth-material.module';
import {ReactiveFormsModule} from '@angular/forms';
import {ProductCommonsModule} from '@product/commons';

@NgModule({
  declarations: [SignInComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    AuthMaterialModule,
    ReactiveFormsModule,
    ProductCommonsModule
  ]
})
export class AuthModule { }
