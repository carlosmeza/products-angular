import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '@product/http/auth/auth.service';
import {SessionService} from '@product/services/session/session.service';
import {TokenModel} from '@product/classes/model/token.model';
import {LoaderComponent} from '@product/components/loader/loader.component';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  authForm: FormGroup;
  showPassword = false;

  @ViewChild(LoaderComponent, {static: true}) loaderComponent: LoaderComponent;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private sessionService: SessionService,
    private router: Router,
    private toastr: ToastrService
  ) {
    this.authForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', Validators.required] // passwordStrong
    });
  }

  get usernameField() {
    return this.authForm.get('username');
  }

  get passwordField() {
    return this.authForm.get('password');
  }

  ngOnInit() {
    this.loaderComponent.hide();
  }

  signIn() {
    if (this.authForm.invalid) { return; }
    this.loaderComponent.show();
    this.authService.signIn(this.authForm.value)
      .subscribe(
        res => {
          if (res.status) {
            const tokenModel = new TokenModel(res.data);
            this.sessionService.create(tokenModel.token);
            // redireccionar
            this.router.navigateByUrl('/admin');
          } else {
            this.toastr.error(res.message, 'Login');
          }
        },
        err => this.loaderComponent.error(err.error.message),
        () => this.loaderComponent.hide()
      );
  }

}
