import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SignInComponent} from './views/sign-in/sign-in.component';
import {GeneralGuard} from '@product/guards/general.guard';

const routes: Routes = [
  { path: '', component: SignInComponent, canActivate: [GeneralGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
