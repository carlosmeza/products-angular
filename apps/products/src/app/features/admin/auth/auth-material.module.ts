import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule
} from '@angular/material';

const MODULES = [
  MatCardModule,
  MatButtonModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule
];

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES]
})
export class AuthMaterialModule { }
