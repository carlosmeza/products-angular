import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ProductModel} from '@product/classes/model/product.model';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FilterRequest} from '@product/classes/request/filter.request';
import {SearchComponent} from '@product/components/search/search.component';
import {LoaderComponent} from '@product/components/loader/loader.component';
import {ProductsService} from '@product/http/products/products.service';
import {merge, of} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements AfterViewInit {

  displayedColumns: string[] = ['id', 'poster', 'poster_detail', 'code', 'name', 'category', 'brand', 'price', 'actions'];
  products: ProductModel[] = [];
  dataSource: MatTableDataSource<ProductModel> = new MatTableDataSource<ProductModel>([]);
  filter: FilterRequest;
  resultsLength = 0;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(SearchComponent, {static: false}) searchComponent: SearchComponent;
  @ViewChild(LoaderComponent, {static: false}) loaderComponent: LoaderComponent;

  constructor(
    private productsHttp: ProductsService
  ) {
    this.filter = new FilterRequest();
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page, this.searchComponent.filter)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.loaderComponent.show();
          this.filter.sort = this.sort.active;
          this.filter.directionSort = this.sort.direction;
          this.filter.page = this.paginator.pageIndex + 1;
          this.filter.items = this.paginator.pageSize;
          return this.productsHttp.getAll(this.filter);
        }),
        map(data => {
          this.loaderComponent.hide();
          this.resultsLength = data.total;
          return data.data;
        }),
        catchError((err) => {
          this.loaderComponent.error();
          return of([]);
        })
      ).subscribe(data =>  {
      this.products = data;
      this.dataSource = new MatTableDataSource<ProductModel>(this.products);
    });
  }

  applyFilter(filterValue: string) {
    this.paginator.pageIndex = 0;
    this.filter.search = filterValue;
  }

}
