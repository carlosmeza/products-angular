import {AfterViewInit, ChangeDetectorRef, Component, Pipe, PipeTransform, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProductModel} from '@product/classes/model/product.model';
import {ProductsService} from '@product/http/products/products.service';
import {LoaderComponent} from '@product/components/loader/loader.component';
import {FormBuilder, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
import {MatBottomSheet, MatBottomSheetRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ListModalComponent} from '@product/components/list-modal/list-modal.component';
import {ToastrService} from 'ngx-toastr';
import {UploadComponent} from '@product/components/upload/upload.component';
import {ProductSpecificationModel} from '@product/classes/model/product-specification.model';
import {FilterRequest} from '@product/classes/request/filter.request';
import {ProductSpecificationService} from '@product/http/product-specification/product-specification.service';
import {merge, of} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {SearchComponent} from '@product/components/search/search.component';
import {BrandModel} from '@product/classes/model/brand.model';

@Component({
  selector: 'app-products-detail',
  templateUrl: './products-detail.component.html',
  styleUrls: ['./products-detail.component.scss']
})
export class ProductsDetailComponent implements AfterViewInit {

  id: number;
  productModel: ProductModel;
  formDetail: FormGroup;
  formSpecification: FormGroup;
  image = '/assets/images/default.png';
  imageDetail = '/assets/images/default-detail.png';
  displayedColumns: string[] = ['id', 'title', 'description'];
  filter: FilterRequest;
  resultsLength = 0;
  newSpecification = true;
  productSpecifications: ProductSpecificationModel[] = [];
  dataSource: MatTableDataSource<ProductSpecificationModel> = new MatTableDataSource<ProductSpecificationModel>([]);

  @ViewChild(LoaderComponent, {static: false}) loaderComponent: LoaderComponent;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(SearchComponent, {static: false}) searchComponent: SearchComponent;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductsService,
    private fb: FormBuilder,
    private bottomSheet: MatBottomSheet,
    private toastr: ToastrService,
    private productSpecificationService: ProductSpecificationService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.formDetail = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      code: ['', Validators.required],
      price: ['', [Validators.required, Validators.pattern('[0-9]+(\\.[0-9][0-9]?)?')]],
      idBrand: [''],
      brand: ['', Validators.required],
      idCategory: [''],
      category: ['', Validators.required]
    });

    this.formSpecification = this.fb.group({
      id: null,
      title: ['', Validators.required],
      description: ['', Validators.required]
    });

    this.filter = new FilterRequest();
  }

  get name() {
    return this.formDetail.get('name');
  }

  get description() {
    return this.formDetail.get('description');
  }

  get code() {
    return this.formDetail.get('code');
  }

  get price() {
    return this.formDetail.get('price');
  }

  get brand() {
    return this.formDetail.get('brand');
  }

  get category() {
    return this.formDetail.get('category');
  }

  get titleSpecification() {
    return this.formSpecification.get('title');
  }

  get descriptionSpecification() {
    return this.formSpecification.get('description');
  }

  ngAfterViewInit(): void {

    this.id = +this.route.snapshot.paramMap.get('id');

    this.loaderComponent.show();
    this.productService.getProduct(this.id).subscribe(
      res => {
        this.productModel = res;

        if (this.productModel.image) {
          this.image = this.productModel.image;
        }
        if (this.productModel.imageDetail) {
          this.imageDetail = this.productModel.imageDetail;
        }

        const {name, description, code, price, image, imageDetail} = this.productModel;
        this.formDetail.patchValue({
          name, description, code, price
        });

        this.formDetail.patchValue({
          idBrand: this.productModel.brand.id,
          brand: this.productModel.brand.name,
          idCategory: this.productModel.category.id,
          category: this.productModel.category.name
        });

        this.initSpecifications();
      },
      err => this.loaderComponent.error(err.error.message),
      () => {
        this.loaderComponent.hide();
      }
    );

  }

  private initSpecifications() {
    this.changeDetectorRef.detectChanges();

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page, this.searchComponent.filter)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.loaderComponent.show();
          this.filter.id = this.id;
          this.filter.sort = this.sort.active;
          this.filter.directionSort = this.sort.direction;
          this.filter.page = this.paginator.pageIndex + 1;
          this.filter.items = this.paginator.pageSize;
          return this.productSpecificationService.getAll(this.filter);
        }),
        map(data => {
          this.loaderComponent.hide();
          this.resultsLength = data.total;
          return data.data;
        }),
        catchError((err) => {
          this.loaderComponent.error();
          return of([]);
        })
      ).subscribe(data => {
      this.productSpecifications = data;
      this.dataSource = new MatTableDataSource<ProductSpecificationModel>(this.productSpecifications);
    });
  }

  saveProduct() {
    if (this.formDetail.valid) {
      this.loaderComponent.show();

      const request = Object.assign(Object.create(this.formDetail.value), this.formDetail.value);
      request.id = this.id;

      this.productService.update(request).subscribe(
        res => this.toastr.success('Se actualizo correctamente', 'Mantenimiento de productos'),
        err => this.loaderComponent.error(err.error.message),
        () => this.loaderComponent.hide()
      );
    }
  }

  saveSpecification(formDirective: FormGroupDirective) {
    if (this.formSpecification.valid) {
      this.loaderComponent.show();

      const request = Object.assign(Object.create(this.formSpecification.value), this.formSpecification.value);
      request.idProduct = this.id;

      if (this.newSpecification) {
        this.productSpecificationService.create(request).subscribe(
          res => this.insertTable(res, 'Se registro correctamente', formDirective),
          err => this.loaderComponent.error(err.error.message),
          () => this.loaderComponent.hide()
        );
      } else {
        this.productSpecificationService.update(request).subscribe(
          res => this.updateTable(res, 'Se actualizo correctamente', formDirective  ),
          err => this.loaderComponent.error(err.error.message),
          () => this.loaderComponent.hide()
        );
      }
    }
  }

  insertTable(data: ProductSpecificationModel, message: string, formDirective: FormGroupDirective) {
    this.productSpecifications.unshift(data);
    this.dataSource = new MatTableDataSource<ProductSpecificationModel>(this.productSpecifications);
    formDirective.resetForm();
    this.formSpecification.reset();
  }

  updateTable(data: ProductSpecificationModel, message: string, formDirective: FormGroupDirective) {
    const index = this.productSpecifications.findIndex(item => item.id === data.id );
    this.productSpecifications[index] = data;
    this.dataSource = new MatTableDataSource<ProductSpecificationModel>(this.productSpecifications);
    this.toastr.success(message, 'Mantenimiento de especificaciones');
    formDirective.resetForm();
    this.formSpecification.reset();
    this.newSpecification = true;
  }

  openSelector(typeModal: number, titleModal: string): void {
    const dialogRef = this.bottomSheet.open(ListModalComponent, {
      data: {type: typeModal, title: titleModal},
    });

    dialogRef.instance.output.subscribe((value: { id: number, name: string }) => {
      if (typeModal === 1) {
        this.formDetail.patchValue({
          idBrand: value.id,
          brand: value.name,
        });
      } else {
        this.formDetail.patchValue({
          idCategory: value.id,
          category: value.name
        });
      }
    });
  }

  openUploader(type: number) {
    const validExtensions = ['jpg', 'png'];
    const dialogRef = this.bottomSheet.open(UploadComponent, {
      data: {extensions: validExtensions}
    });

    dialogRef.instance.output.subscribe((value: { file }) => {
      this.uploadFile(value.file, type, dialogRef);
    });
  }

  uploadFile(file: File, type: number, ref: MatBottomSheetRef<UploadComponent, any>) {
    this.productService.upload(this.id, type, file).subscribe(
      res => {
        this.toastr.success('Se subio la imagen correctamente', 'Carga de imagenes');
        if (type === 1) {
          this.productModel.image = res.url;
          this.image = res.url;
        } else {
          this.productModel.imageDetail = res.url;
          this.imageDetail = res.url;
        }
      },
      err => {
        ref.instance.loadHide();
      },
      () => ref.instance.close()
    );
  }

  applyFilter(filterValue: string) {
    this.paginator.pageIndex = 0;
    this.filter.search = filterValue;
  }

  cancelNewSubscription() {
    this.formSpecification.reset();
    this.newSpecification = true;
  }

  selectSpecification(productSpecification: ProductSpecificationModel) {
    const { id, title, description} = productSpecification;
    this.formSpecification.patchValue({
      id, title, description
    });
    this.newSpecification = false;
  }
}

@Pipe({name: 'split'})
export class SplitPipe implements PipeTransform {
  transform(value: string, [separator]): string {
    const splits = value.split(separator);
    if (splits.length > 1) {
      return splits.pop();
    } else {
      return '';
    }
  }
}
