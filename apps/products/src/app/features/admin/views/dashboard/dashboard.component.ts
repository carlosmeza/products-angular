import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {DashboardModel} from '@product/classes/model/dashboard.model';
import {DashboardService} from '@product/http/dashboard/dashboard.service';
import {LoaderComponent} from '@product/components/loader/loader.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements AfterViewInit {

  dashboard: DashboardModel;
  @ViewChild(LoaderComponent, {static: false}) loaderComponent: LoaderComponent;

  constructor(private dashboardService: DashboardService) {
    this.dashboard = new DashboardModel();
  }

  ngAfterViewInit(): void{
    this.loaderComponent.show();

    this.dashboardService.get().subscribe(
      res => this.dashboard = res,
      err => this.loaderComponent.error(err.error.message),
      () => this.loaderComponent.hide()
    );
  }
}
