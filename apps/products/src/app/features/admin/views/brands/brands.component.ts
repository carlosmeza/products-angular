import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {BrandsService} from '@product/http/brands/brands.service';
import {BrandFormComponent} from '../../components/forms/brand-form/brand-form.component';
import {BrandRequest} from '@product/classes/request/brand.request';
import {BrandModel} from '@product/classes/model/brand.model';
import {merge, of} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {FilterRequest} from '@product/classes/request/filter.request';
import {SearchComponent} from '@product/components/search/search.component';
import {LoaderComponent} from '@product/components/loader/loader.component';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.scss']
})
export class BrandsComponent implements AfterViewInit  {

  displayedColumns: string[] = ['id', 'name', 'description', 'slug', 'actions'];
  brands: BrandModel[] = [];
  dataSource: MatTableDataSource<BrandModel> = new MatTableDataSource<BrandModel>([]);
  filter: FilterRequest;
  resultsLength = 0;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(SearchComponent, {static: false}) searchComponent: SearchComponent;
  @ViewChild(LoaderComponent, {static: false}) loaderComponent: LoaderComponent;

  constructor(
    private brandsHttp: BrandsService,
    private dialog: MatDialog,
    private toastr: ToastrService
  ) {
    this.filter = new FilterRequest();
  }

  openForm(brand?: BrandModel): void {
    const dialogRef = this.dialog.open(BrandFormComponent, {
      width: '350px',
      data: {brand}
    });

    dialogRef.afterClosed().subscribe((data?: BrandModel) => {
      if (data) {
        data.id ? this.update(data) : this.create(data);
      }
    });
  }

  create(data: BrandRequest) {
    this.loaderComponent.show();
    this.brandsHttp.create(data)
      .subscribe(
        res => {
          this.brands.unshift(res);
          this.dataSource = new MatTableDataSource<BrandModel>(this.brands);
          this.toastr.success('Se registro correctamente', 'Mantenimiento de marcas');
        },
        err => this.loaderComponent.error(),
        () => this.loaderComponent.hide()
      );
  }

  update(data: BrandRequest) {
    this.loaderComponent.show();
    this.brandsHttp.update(data)
      .subscribe(
        res => {
          const index = this.brands.findIndex(item => item.id === data.id );
          this.brands[index] = res;
          this.dataSource = new MatTableDataSource<BrandModel>(this.brands);
          this.toastr.success('Se actualizo correctamente', 'Mantenimiento de marcas');
        },
        err => this.loaderComponent.error(),
        () => this.loaderComponent.hide()
      );
  }

  ngAfterViewInit(): void {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page, this.searchComponent.filter)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.loaderComponent.show();
          this.filter.sort = this.sort.active;
          this.filter.directionSort = this.sort.direction;
          this.filter.page = this.paginator.pageIndex + 1;
          this.filter.items = this.paginator.pageSize;
          return this.brandsHttp.getAll(this.filter);
        }),
        map(data => {
          this.loaderComponent.hide();
          this.resultsLength = data.total;
          return data.data;
        }),
        catchError((err) => {
          this.loaderComponent.error();
          return of([]);
        })
      ).subscribe(data =>  {
        this.brands = data;
        this.dataSource = new MatTableDataSource<BrandModel>(this.brands);
    });
  }

  applyFilter(filterValue: string) {
    this.paginator.pageIndex = 0;
    this.filter.search = filterValue;
  }

}
