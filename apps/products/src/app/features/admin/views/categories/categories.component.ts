import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {CategoryModel} from '@product/classes/model/category.model';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FilterRequest} from '@product/classes/request/filter.request';
import {SearchComponent} from '@product/components/search/search.component';
import {LoaderComponent} from '@product/components/loader/loader.component';
import {CategoriesService} from '@product/http/categories/categories.service';
import {CategoryFormComponent} from '../../components/forms/category-form/category-form.component';
import {CategoryRequest} from '@product/classes/request/category.request';
import {merge, of} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements AfterViewInit {

  displayedColumns: string[] = ['id', 'name', 'slug', 'actions'];
  categories: CategoryModel[] = [];
  dataSource: MatTableDataSource<CategoryModel> = new MatTableDataSource<CategoryModel>([]);
  filter: FilterRequest;
  resultsLength = 0;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(SearchComponent, {static: false}) searchComponent: SearchComponent;
  @ViewChild(LoaderComponent, {static: false}) loaderComponent: LoaderComponent;

  constructor(
    private categoriesHttp: CategoriesService,
    private dialog: MatDialog,
    private toastr: ToastrService
  ) {
    this.filter = new FilterRequest();
  }

  openForm(category?: CategoryModel): void {
    const dialogRef = this.dialog.open(CategoryFormComponent, {
      width: '350px',
      data: {category}
    });

    dialogRef.afterClosed().subscribe((data?: CategoryModel) => {
      if (data) {
        data.id ? this.update(data) : this.create(data);
      }
    });
  }

  create(data: CategoryRequest) {
    this.loaderComponent.show();
    this.categoriesHttp.create(data)
      .subscribe(
        res => {
          this.categories.unshift(res);
          this.dataSource = new MatTableDataSource<CategoryModel>(this.categories);
          this.toastr.success('Se registro correctamente', 'Mantenimiento de categorias');
        },
        err => this.loaderComponent.error(),
        () => this.loaderComponent.hide()
      );
  }

  update(data: CategoryRequest) {
    this.loaderComponent.show();
    this.categoriesHttp.update(data)
      .subscribe(
        res => {
          const index = this.categories.findIndex(item => item.id === data.id );
          this.categories[index] = res;
          this.dataSource = new MatTableDataSource<CategoryModel>(this.categories);
          this.toastr.success('Se actualizo correctamente', 'Mantenimiento de categorias');
        },
        err => this.loaderComponent.error(),
        () => this.loaderComponent.hide()
      );
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page, this.searchComponent.filter)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.loaderComponent.show();
          this.filter.sort = this.sort.active;
          this.filter.directionSort = this.sort.direction;
          this.filter.page = this.paginator.pageIndex + 1;
          this.filter.items = this.paginator.pageSize;
          return this.categoriesHttp.getAll(this.filter);
        }),
        map(data => {
          this.loaderComponent.hide();
          this.resultsLength = data.total;
          return data.data;
        }),
        catchError((err) => {
          this.loaderComponent.error();
          return of([]);
        })
      ).subscribe(data =>  {
      this.categories = data;
      this.dataSource = new MatTableDataSource<CategoryModel>(this.categories);
    });
  }

  applyFilter(filterValue: string) {
    this.paginator.pageIndex = 0;
    this.filter.search = filterValue;
  }

}
