import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { BrandsComponent } from './views/brands/brands.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import {AdminMaterialModule} from './admin-material.module';
import {AdminGuardsModule, ProductCommonsModule} from '@product/commons';
import {ComponentsAdminModule} from './components/components-admin.module';
import {CategoriesComponent} from './views/categories/categories.component';
import {ProductsComponent} from './views/products/products.component';
import {ProductsDetailComponent, SplitPipe} from './views/products/products-detail/products-detail.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ListModalComponent} from '@product/components/list-modal/list-modal.component';
import {UploadComponent} from '@product/components/upload/upload.component';


@NgModule({
  declarations: [
    AdminComponent,
    DashboardComponent,
    BrandsComponent,
    CategoriesComponent,
    ProductsComponent,
    ProductsDetailComponent,
    SplitPipe
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    AdminMaterialModule,
    AdminGuardsModule,
    ComponentsAdminModule,
    ProductCommonsModule,
    ReactiveFormsModule,
  ],
  entryComponents: [
    ListModalComponent,
    UploadComponent
  ]
})
export class AdminModule { }
