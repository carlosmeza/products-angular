import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {BrandModel} from '@product/classes/model/brand.model';

@Component({
  selector: 'app-brand-form',
  templateUrl: './brand-form.component.html',
  styleUrls: ['./brand-form.component.scss']
})
export class BrandFormComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<BrandFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { brand?: BrandModel }
  ) {
    this.form = this.fb.group({
      id: null,
      name: ['', Validators.required],
      description: ['', Validators.required],
      slug: ['', Validators.required]
    });
  }

  get name() {
    return this.form.get('name');
  }

  get description() {
    return this.form.get('description');
  }

  get slug() {
    return this.form.get('slug');
  }

  save() {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }

  cancel() {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.data.brand) {
      const { id, name, description, slug } = this.data.brand;
      this.form.patchValue({
        id, name, description, slug
      });
    }
  }

}
