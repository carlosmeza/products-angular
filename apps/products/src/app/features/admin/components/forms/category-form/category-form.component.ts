import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {CategoryModel} from '@product/classes/model/category.model';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss']
})
export class CategoryFormComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<CategoryFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { category?: CategoryModel }
  ) {
    this.form = this.fb.group({
      id: null,
      name: ['', Validators.required],
      slug: ['', Validators.required]
    });
  }

  get name() {
    return this.form.get('name');
  }

  get slug() {
    return this.form.get('slug');
  }

  save() {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }

  cancel() {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.data.category) {
      const { id, name, slug } = this.data.category;
      this.form.patchValue({
        id, name, slug
      });
    }
  }

}
