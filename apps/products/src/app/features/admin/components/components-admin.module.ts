import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrandFormComponent } from './forms/brand-form/brand-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AdminMaterialModule} from '../admin-material.module';
import { AdminHeaderComponent } from './layout/admin-header/admin-header.component';
import { AdminSidebarComponent } from './layout/admin-sidebar/admin-sidebar.component';
import {AdminRoutingModule} from '../admin-routing.module';
import { CategoryFormComponent } from './forms/category-form/category-form.component';

const COMPONENTS = [
  AdminHeaderComponent,
  AdminSidebarComponent,
  BrandFormComponent,
  CategoryFormComponent
];

@NgModule({
  entryComponents: [
    BrandFormComponent,
    CategoryFormComponent
  ],
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  imports: [
    CommonModule,
    AdminMaterialModule,
    ReactiveFormsModule,
    AdminRoutingModule
  ]
})
export class ComponentsAdminModule { }
