import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SessionService} from '@product/services/session/session.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class AdminHeaderComponent implements OnInit {

  @Output() event: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private sessionService: SessionService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  sidenavAction() {
    this.event.emit();
  }

  logout() {
    this.sessionService.destroy();
    this.router.navigateByUrl('/admin/login');
  }

}
