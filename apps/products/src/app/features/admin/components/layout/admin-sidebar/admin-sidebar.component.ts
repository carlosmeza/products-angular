import {Component, Input, OnInit} from '@angular/core';
import {MenuModel} from '@product/classes/model/menu.model';

@Component({
  selector: 'app-admin-sidebar',
  templateUrl: './admin-sidebar.component.html',
  styleUrls: ['./admin-sidebar.component.scss']
})
export class AdminSidebarComponent implements OnInit {

  @Input() menu: MenuModel[] = [];

  constructor() { }

  ngOnInit() {
  }

}
