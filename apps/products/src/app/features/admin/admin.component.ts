import {Component, OnInit, ViewChild} from '@angular/core';
import {MenuModel} from '@product/classes/model/menu.model';
import {MatSidenav} from '@angular/material';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  menuOptions: MenuModel[] = [
    { icon: 'home', title: 'Inicio', path: ['./'] },
    { icon: 'list_alt', title: 'Marcas', path: ['./marcas'] },
    { icon: 'assignment', title: 'Categorias', path: ['./categorias'] },
    { icon: 'shopping_basket', title: 'Productos', path: ['./productos'] }
  ];

  @ViewChild(MatSidenav, {static: false}) sidenav: MatSidenav;
  opened: boolean;

  constructor() {
    this.opened = true;
  }

  ngOnInit() {
  }

  sidenavAction() {
    this.sidenav.toggle();
  }

}
