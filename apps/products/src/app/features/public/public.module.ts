import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import {PublicMaterialModule} from './public-material.module';
import {ProductCommonsModule} from '@product/commons';
import {CatalogComponent} from './views/catalog/catalog.component';
import {CatalogDetailComponent} from './views/catalog-detail/catalog-detail.component';

@NgModule({
  declarations: [
    PublicComponent,
    CatalogComponent,
    CatalogDetailComponent
  ],
  imports: [
    CommonModule,
    ProductCommonsModule,
    PublicRoutingModule,
    PublicMaterialModule
  ]
})
export class PublicModule { }
