import {AfterViewInit, Component, EventEmitter, OnInit, ViewChild} from '@angular/core';
import {MatDrawer, MatListOption, MatPaginator, MatSelectionList, MatSidenav, MatTableDataSource} from '@angular/material';
import {FilterRequest} from '@product/classes/request/filter.request';
import {CategoryModel} from '@product/classes/model/category.model';
import {LoaderComponent} from '@product/components/loader/loader.component';
import {CategoriesService} from '@product/http/categories/categories.service';
import {merge, of} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {ProductModel} from '@product/classes/model/product.model';
import {ProductsService} from '@product/http/products/products.service';
import {CatalogDetailComponent} from '../catalog-detail/catalog-detail.component';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements AfterViewInit {

  @ViewChild(MatSidenav, {static: false}) sidenav: MatSidenav;
  opened: boolean;
  filterCategories: FilterRequest;
  filterProducts: FilterRequest;
  categories: CategoryModel[] = [];
  categoriesSelected: CategoryModel[] = [];
  products: ProductModel[] = [];
  selectAll: boolean;
  resultsLength = 0;
  changeFilter: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('loaderSidebar', {static: false}) loaderSidebar: LoaderComponent;
  @ViewChild('loaderCatalog', {static: false}) loaderCatalog: LoaderComponent;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild('catalogDetail', {static: false}) catalogDetail: CatalogDetailComponent;

  constructor(
    private categoriesService: CategoriesService,
    private productsService: ProductsService
  ) {
    this.opened = true;
    this.filterCategories = new FilterRequest();
    this.filterCategories.sort = 'name';
    this.filterCategories.directionSort = 'desc';
    this.filterProducts = new FilterRequest();
    this.filterProducts.sort = 'name';
    this.filterProducts.directionSort = 'desc';
    this.selectAll = true;
  }

  sidenavAction() {
    this.sidenav.toggle();
  }

  ngAfterViewInit(): void {
    this.loaderSidebar.show();
    this.categoriesService.getAll(this.filterCategories).subscribe(
      res => this.categories = res.data,
      err => this.loaderSidebar.error(err.error.message),
      () => this.loaderSidebar.hide()
    );

    this.paginator.pageIndex = 0;

    merge(this.paginator.page, this.changeFilter)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.loaderCatalog.show();
          this.filterProducts.page = this.paginator.pageIndex + 1;
          this.filterProducts.items = this.paginator.pageSize;
          return this.productsService.getAll(this.filterProducts);
        }),
        map(data => {
          this.loaderCatalog.hide();
          this.resultsLength = data.total;
          return data.data;
        }),
        catchError((err) => {
          this.loaderCatalog.error();
          return of([]);
        })
      ).subscribe(data =>  {
      this.products = data;
    });
  }

  changeAll(list: MatSelectionList, optionAll: MatListOption) {
    if (!optionAll.disabled) {
      this.selectAll = optionAll.selected;
      if (optionAll.selected) { // Deseleccionar
        list.deselectAll();
        optionAll.disabled = true;
        optionAll.selected = true;
        this.categoriesSelected = [];
        this.getProducts();
      }
    }
  }

  changeCategories(list: MatSelectionList, optionAll: MatListOption) {
    this.categoriesSelected = list.selectedOptions.selected.filter(item => item.value).map(item => item.value);
    if (this.categoriesSelected.length === 0) {
      optionAll.disabled = true;
      optionAll.selected = true;
    } else {
      optionAll.disabled = false;
      optionAll.selected = false;
    }
    this.getProducts();
  }

  getProducts() {
    this.filterProducts.categories = this.categoriesSelected.map(item => item.id).join(',');
    this.paginator.pageIndex = 0;
    this.changeFilter.emit();
  }

  applyFilter(filterValue: string) {
    this.paginator.pageIndex = 0;
    this.filterProducts.search = filterValue;
    this.changeFilter.emit();
  }

  loadDetail(matDrawer: MatDrawer, productModel: ProductModel) {
    matDrawer.toggle();
    this.catalogDetail.initProduct(productModel);
  }

}
