import {Component, Input, OnInit} from '@angular/core';
import {ProductModel} from '@product/classes/model/product.model';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-catalog-detail',
  templateUrl: './catalog-detail.component.html',
  styleUrls: ['./catalog-detail.component.scss']
})
export class CatalogDetailComponent implements OnInit {

  productModel: ProductModel;
  constructor(private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  initProduct(productModel: ProductModel) {
    this.productModel = productModel;
  }

  showMessage(message: string) {
    this.snackBar.open(message, null, {
      duration: 1500,
    });
  }

}
