import { Injectable } from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {environment} from '../../../../../apps/products/src/environments/environment';
import {SessionService} from '@product/services/session/session.service';

@Injectable()
export class CommonInterceptor implements HttpInterceptor {

  constructor(
    private toastr: ToastrService,
    private session: SessionService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let headers = {};

    if (this.session.token) {
      headers = {
        Authorization: this.session.token
      };
    }
    const request = req.clone({
      url: `${environment.API_URL}/${req.url}`,
      setHeaders: headers
    });

    return next.handle(request)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          if (err.error && err.error.message) {
            this.toastr.error(err.error.message, 'Ups! Ocurrio un error');
          }
          return throwError(err);
        })
      );
  }

}
