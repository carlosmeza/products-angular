import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BrandRespose} from '@product/classes/response/brand.response';
import {Observable, of} from 'rxjs';
import {ApiResponse} from '@product/classes/response/api.response';
import {map} from 'rxjs/operators';
import {BrandRequest} from '@product/classes/request/brand.request';
import {BrandModel} from '@product/classes/model/brand.model';
import {FilterRequest} from '@product/classes/request/filter.request';
import {GenericService} from '@product/http/generic.service';

@Injectable()
export class BrandsService implements GenericService {

  private endpoind = 'brand';

  constructor(private http: HttpClient) { }

  getAll(filter: FilterRequest): Observable<ApiResponse<BrandModel[]>> {
    return this.http.get<ApiResponse<BrandRespose[]>>(this.endpoind, {params: filter.params});
  }

  create(data: BrandRequest): Observable<BrandModel> {
    return this.http.post<ApiResponse<BrandRespose>>(this.endpoind, data)
      .pipe(map((item: ApiResponse<BrandRespose>) => new BrandModel(item.data) ));
  }

  update(data: BrandRequest): Observable<BrandModel> {
    return this.http.put<ApiResponse<BrandRespose>>(this.endpoind, data)
      .pipe(map((item: ApiResponse<BrandRespose>) => new BrandModel(item.data) ));
  }
}
