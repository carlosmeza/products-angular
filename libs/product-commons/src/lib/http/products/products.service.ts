import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FilterRequest} from '@product/classes/request/filter.request';
import {Observable} from 'rxjs';
import {ApiResponse} from '@product/classes/response/api.response';
import {ProductModel} from '@product/classes/model/product.model';
import {map} from 'rxjs/operators';
import {ProductRespose} from '@product/classes/response/product.response';
import {ProductRequest} from '@product/classes/request/product.request';
import {UploadModel} from '@product/classes/model/upload.model';
import {UploadResponse} from '@product/classes/response/upload.response';

@Injectable()
export class ProductsService {
  private endpoind = 'product';
  constructor(private http: HttpClient) { }

  getAll(filter: FilterRequest): Observable<ApiResponse<ProductModel[]>> {
    return this.http.get<ApiResponse<ProductModel[]>>(this.endpoind, {params: filter.params});
  }

  getProduct(id: number): Observable<ProductModel> {
    return this.http.get<ApiResponse<ProductModel>>(this.endpoind + '/' + id)
      .pipe(map((item: ApiResponse<ProductRespose>) => new ProductModel(item.data)));
  }

  update(data: ProductRequest): Observable<ProductModel> {
    return this.http.put<ApiResponse<ProductModel>>(this.endpoind, data)
      .pipe(map((item: ApiResponse<ProductRespose>) => new ProductModel(item.data)));
  }

  upload(id: number, type: number, file: File): Observable<UploadModel> {
    const formData = new FormData();
    formData.append('id', id + '');
    formData.append('type', type + '');
    formData.append('file', file);

    return this.http.patch<ApiResponse<UploadModel>>(this.endpoind, formData)
      .pipe(map((item: ApiResponse<UploadResponse>) => new UploadModel(item.data)));
  }
}
