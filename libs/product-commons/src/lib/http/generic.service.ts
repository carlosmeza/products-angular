import {FilterRequest} from '@product/classes/request/filter.request';
import {Observable, of} from 'rxjs';
import {ApiResponse} from '@product/classes/response/api.response';

export class GenericService {
  getAll(filter: FilterRequest): Observable<ApiResponse<object>> {
    return of<ApiResponse<object>>();
  }
}
