import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AuthService} from './auth/auth.service';
import {BrandsService} from './brands/brands.service';
import {CategoriesService} from '@product/http/categories/categories.service';
import {ProductsService} from '@product/http/products/products.service';
import {ProductSpecificationService} from '@product/http/product-specification/product-specification.service';
import {DashboardService} from '@product/http/dashboard/dashboard.service';

@NgModule({
  imports: [
    HttpClientModule
  ],
  providers: [
    AuthService,
    BrandsService,
    CategoriesService,
    ProductsService,
    ProductSpecificationService,
    DashboardService
  ]
})
export class HttpCommonsModule { }
