import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FilterRequest} from '@product/classes/request/filter.request';
import {Observable} from 'rxjs';
import {ApiResponse} from '@product/classes/response/api.response';
import {CategoryModel} from '@product/classes/model/category.model';
import {map} from 'rxjs/operators';
import {CategoryRequest} from '@product/classes/request/category.request';
import {CategoryRespose} from '@product/classes/response/category.response';
import {GenericService} from '@product/http/generic.service';

@Injectable()
export class CategoriesService implements GenericService {

  private endpoind = 'category';

  constructor(private http: HttpClient) {
  }

  getAll(filter: FilterRequest): Observable<ApiResponse<CategoryModel[]>> {
    return this.http.get<ApiResponse<CategoryModel[]>>(this.endpoind, {params: filter.params});
  }

  create(data: CategoryRequest): Observable<CategoryModel> {
    return this.http.post<ApiResponse<CategoryRespose>>(this.endpoind, data)
      .pipe(map((item: ApiResponse<CategoryRespose>) => new CategoryModel(item.data)));
  }

  update(data: CategoryRequest): Observable<CategoryModel> {
    return this.http.put<ApiResponse<CategoryRespose>>(this.endpoind, data)
      .pipe(map((item: ApiResponse<CategoryRespose>) => new CategoryModel(item.data)));
  }
}
