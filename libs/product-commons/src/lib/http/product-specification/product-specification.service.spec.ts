import { TestBed } from '@angular/core/testing';

import { ProductSpecificationService } from './product-specification.service';

describe('ProductSpecificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductSpecificationService = TestBed.get(ProductSpecificationService);
    expect(service).toBeTruthy();
  });
});
