import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FilterRequest} from '@product/classes/request/filter.request';
import {Observable} from 'rxjs';
import {ApiResponse} from '@product/classes/response/api.response';
import {ProductSpecificationModel} from '@product/classes/model/product-specification.model';
import {ProductSpecificationRequest} from '@product/classes/request/product-specification.request';
import {ProductSpecificationResponse} from '@product/classes/response/product-specification.response';
import {map} from 'rxjs/operators';

@Injectable()
export class ProductSpecificationService {
  private endpoind = 'product/specification';
  constructor(private http: HttpClient) { }

  getAll(filter: FilterRequest): Observable<ApiResponse<ProductSpecificationModel[]>> {
    return this.http.get<ApiResponse<ProductSpecificationModel[]>>(this.endpoind, {params: filter.params});
  }

  create(data: ProductSpecificationRequest): Observable<ProductSpecificationModel> {
    return this.http.post<ApiResponse<ProductSpecificationModel>>(this.endpoind, data)
      .pipe(map((item: ApiResponse<ProductSpecificationResponse>) => new ProductSpecificationModel(item.data)));
  }

  update(data: ProductSpecificationRequest): Observable<ProductSpecificationModel> {
    return this.http.put<ApiResponse<ProductSpecificationModel>>(this.endpoind, data)
      .pipe(map((item: ApiResponse<ProductSpecificationResponse>) => new ProductSpecificationModel(item.data)));
  }

}
