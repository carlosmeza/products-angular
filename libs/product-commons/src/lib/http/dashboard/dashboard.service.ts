import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiResponse} from '@product/classes/response/api.response';
import {DashboardModel} from '@product/classes/model/dashboard.model';
import {DashboardResponse} from '@product/classes/response/dashboard.response';
import {map} from 'rxjs/operators';

@Injectable()
export class DashboardService {
  private endpoind = 'dashboard';

  constructor(private http: HttpClient) {
  }

  get(): Observable<DashboardModel> {
    return this.http.get<ApiResponse<DashboardModel>>(this.endpoind)
      .pipe(map((item: ApiResponse<DashboardResponse>) => new DashboardModel(item.data)));
  }
}
