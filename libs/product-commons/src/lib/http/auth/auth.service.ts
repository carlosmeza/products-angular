import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TokenRequest} from '@product/classes/request/token.request';
import {ApiResponse} from '@product/classes/response/api.response';
import {Observable} from 'rxjs';
import {TokenRespose} from '@product/classes/response/token.response';

@Injectable()
export class AuthService {
  private endpoind = 'auth/token';

  constructor(
    private http: HttpClient
  ) { }

  signIn(credentials: TokenRequest): Observable<ApiResponse<TokenRespose>> {
    return this.http.post<ApiResponse<TokenRespose>>(this.endpoind, credentials);
  }
}
