import {NgModule} from '@angular/core';
import {CryptoService} from './crypto/crypto.service';
import {SessionService} from './session/session.service';

@NgModule({
  providers: [
    SessionService,
    CryptoService
  ]
})
export class ServicesModule {
}
