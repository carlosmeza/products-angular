import { NgModule } from '@angular/core';
import {ServicesModule} from './services/services.module';
import {InterceptorsModule} from './interceptors/interceptors.module';
import {HttpCommonsModule} from './http/http.module';
import {ComponentsModule} from './components/components.module';

const MODULES = [
  ServicesModule,
  InterceptorsModule,
  HttpCommonsModule,
  ComponentsModule
];

@NgModule({
  imports: [
    ...MODULES
  ],
  exports: [
    ...MODULES
  ]
})
export class ProductCommonsModule { }
