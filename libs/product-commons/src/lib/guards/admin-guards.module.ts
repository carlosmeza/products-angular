import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {ServicesModule} from '../services/services.module';
import {AdminGuard} from '@product/guards/admin.guard';
import {GeneralGuard} from '@product/guards/general.guard';

@NgModule({
  imports: [
    ServicesModule,
    RouterModule
  ],
  providers: [
    AdminGuard,
    GeneralGuard
  ]
})
export class AdminGuardsModule { }
