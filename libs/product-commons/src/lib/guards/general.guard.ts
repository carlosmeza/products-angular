import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {SessionService} from '@product/services/session/session.service';

@Injectable()
export class GeneralGuard implements CanActivate {

  constructor(
    private session: SessionService,
    private router: Router
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const isAuthenticated = !!this.session.token;
    if (isAuthenticated && state.url.endsWith('/login')) {
      this.router.navigateByUrl('/admin');
      return false;
    }
    return true;
  }

}
