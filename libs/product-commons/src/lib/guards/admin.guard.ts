import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {SessionService} from '../services/session/session.service';

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(
    private session: SessionService,
    private router: Router
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const isAuthenticated = !!this.session.token;
    if (!isAuthenticated) {
      this.router.navigateByUrl('/admin/login');
    }
    return isAuthenticated;
  }

}
