import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  isLoading = true;
  isError = false;
  message = 'Ocurrio un error al obtener la informacion...';

  constructor() { }

  ngOnInit() {
  }

  show() {
    this.isLoading = true;
    this.isError = false;
  }

  hide() {
    this.isLoading = false;
    this.isError = false;
  }

  error(message?: string) {
    this.isLoading = false;
    this.isError = true;
    if (message) {
      this.message = message;
    }
  }

}
