import {AfterViewInit, Component, EventEmitter, Inject, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material';
import {LoaderComponent} from '@product/components/loader/loader.component';
import {validExtension} from '@product/utils/validators';

@Component({
  selector: 'lib-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements AfterViewInit {

  form: FormGroup;
  output: EventEmitter<{file}> = new EventEmitter<{file}>();

  @ViewChild(LoaderComponent, {static: false}) loaderComponent: LoaderComponent;

  constructor(
    private fb: FormBuilder,
    private bottomSheetRef: MatBottomSheetRef<UploadComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: {extensions: string[]}
  ) {
    this.form = this.fb.group({
      file: ['', [Validators.required, validExtension(this.data.extensions)]],
    });

  }

  get fileField() {
    return this.form.get('file');
  }

  save() {
    if (this.form.valid) {
      this.loadShow();
      this.output.emit(this.form.value);
    }
  }

  onFileChange(event) {
    // form data
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.form.patchValue({
        file
      });
    }
  }

  close() {
    this.loadHide();
    this.bottomSheetRef.dismiss();
  }

  loadShow() {
    this.loaderComponent.show();
  }

  loadHide() {
    this.loaderComponent.hide();
  }

  loadError(message: string) {
    this.loaderComponent.error(message);
    this.loaderComponent.hide();
  }

  ngAfterViewInit(): void {
    this.loaderComponent.hide();
  }

}
