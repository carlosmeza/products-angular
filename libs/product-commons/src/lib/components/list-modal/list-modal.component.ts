import {AfterViewInit, Component, EventEmitter, Inject, ViewChild} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FilterRequest} from '@product/classes/request/filter.request';
import {SearchComponent} from '@product/components/search/search.component';
import {LoaderComponent} from '@product/components/loader/loader.component';
import {BrandsService} from '@product/http/brands/brands.service';
import {CategoriesService} from '@product/http/categories/categories.service';

@Component({
  selector: 'lib-list-modal',
  templateUrl: './list-modal.component.html',
  styleUrls: ['./list-modal.component.css']
})
export class ListModalComponent implements AfterViewInit {

  displayedColumns: string[] = ['id', 'name'];
  list: ListModel[] = [];
  dataSource: MatTableDataSource<ListModel> = new MatTableDataSource<ListModel>([]);
  filter: FilterRequest;
  resultsLength = 0;
  loaded = false;
  output: EventEmitter<ListModel> = new EventEmitter<ListModel>();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(SearchComponent, {static: false}) searchComponent: SearchComponent;
  @ViewChild(LoaderComponent, {static: false}) loaderComponent: LoaderComponent;

  constructor(
    private bottomSheetRef: MatBottomSheetRef<ListModalComponent>,
    private brandsService: BrandsService,
    private categoriesService: CategoriesService,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: {type: number, title: string}
  ) {
    this.filter = new FilterRequest();
  }

  ngAfterViewInit(): void {

    this.loaderComponent.show();

    if (this.data.type === 1) { // Brands
      this.brandsService.getAll(this.filter).subscribe(
        res => {
          this.list = res.data as ListModel[];
          this.resetDataSource();
        },
        err => this.loaderComponent.error(err.error.message),
        () => this.loaderComponent.hide()
      );
    } else {
      this.categoriesService.getAll(this.filter).subscribe(
        res => {
          this.list = res.data as ListModel[];
          this.resetDataSource();
        },
        err => this.loaderComponent.error(err.error.message),
        () => this.loaderComponent.hide()
      );
    }
  }

  applyFilter(filterValue: string) {
    this.paginator.pageIndex = 0;
    this.filter.search = filterValue;

    filterValue = filterValue.trim().toLowerCase();
    const items = this.list.filter(i => {
      return (i.name.toLowerCase().includes(filterValue));
    });

    this.resetDataSource(filterValue ? items : null);
  }

  resetDataSource(values?) {
    this.dataSource = new MatTableDataSource<ListModel>(values ? values : this.list);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loaded = true;
  }

  selectItem(listModel: ListModel) {
    this.output.emit(listModel);
    this.bottomSheetRef.dismiss();
  }
}

interface ListModel {
  id: number;
  name: string;
}
