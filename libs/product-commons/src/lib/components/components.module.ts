import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search/search.component';
import {ComponentsMaterialModule} from './components-material.module';
import {ReactiveFormsModule} from '@angular/forms';
import {LoaderComponent} from '@product/components/loader/loader.component';
import {ListModalComponent} from '@product/components/list-modal/list-modal.component';
import {UploadComponent} from '@product/components/upload/upload.component';

const COMPONENTS = [
  SearchComponent,
  LoaderComponent,
  ListModalComponent,
  UploadComponent
];

@NgModule({
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  imports: [
    CommonModule,
    ComponentsMaterialModule,
    ReactiveFormsModule
  ]
})
export class ComponentsModule { }
