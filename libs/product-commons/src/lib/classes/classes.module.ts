import { NgModule } from '@angular/core';
import {TokenRespose} from './response/token.response';
import {TokenRequest} from './request/token.request';
import {ApiResponse} from './response/api.response';
import {BrandRequest} from './request/brand.request';
import {BrandRespose} from './response/brand.response';
import {BrandModel} from './model/brand.model';
import {TokenModel} from './model/token.model';
import {UserModel} from './model/user.model';
import {FilterRequest} from '@product/classes/request/filter.request';
import {MenuModel} from '@product/classes/model/menu.model';
import {CategoryRespose} from '@product/classes/response/category.response';
import {CategoryModel} from '@product/classes/model/category.model';
import {CategoryRequest} from '@product/classes/request/category.request';
import {ProductRespose} from '@product/classes/response/product.response';
import {ProductSpecificationResponse} from '@product/classes/response/product-specification.response';
import {ProductModel} from '@product/classes/model/product.model';
import {ProductSpecificationModel} from '@product/classes/model/product-specification.model';
import {UploadModel} from '@product/classes/model/upload.model';
import {UploadResponse} from '@product/classes/response/upload.response';
import {ProductSpecificationRequest} from '@product/classes/request/product-specification.request';
import {DashboardModel} from '@product/classes/model/dashboard.model';
import {DashboardResponse} from '@product/classes/response/dashboard.response';

@NgModule({
  exports: [
    // Request
    TokenRequest,
    BrandRequest,
    FilterRequest,
    CategoryRequest,
    ProductSpecificationRequest,

    // Response
    TokenRespose,
    ApiResponse,
    BrandRespose,
    CategoryRespose,
    ProductRespose,
    ProductSpecificationResponse,
    UploadResponse,
    DashboardResponse,

    // Model
    UserModel,
    MenuModel,
    BrandModel,
    TokenModel,
    CategoryModel,
    ProductModel,
    ProductSpecificationModel,
    UploadModel,
    DashboardModel
  ]
})
export class ClassesModule { }
