import {CategoryRespose} from '@product/classes/response/category.response';

export class CategoryRequest implements CategoryRespose {
  id: number;
  name: string;
  slug: string;
  image: string;
}
