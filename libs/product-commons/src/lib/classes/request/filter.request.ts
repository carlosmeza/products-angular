import {HttpParams} from '@angular/common/http';

export class FilterRequest {
  id?: number;
  sort?: string;
  directionSort?: string;
  page?: number;
  items?: number;
  search?: string;
  categories?: string;

  get params(): HttpParams {
    return Object.entries(this).reduce(
      (params, [key, value]) => params.set(key, value), new HttpParams()
    );
  }
}
