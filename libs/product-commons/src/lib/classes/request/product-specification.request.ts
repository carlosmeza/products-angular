export class ProductSpecificationRequest {
  id: number;
  idProduct: number;
  title: string;
  description: string;
}
