export class ProductRequest {
  id: number;
  idBrand: number;
  idCategory: number;
  name: string;
  description: string;
  price: number;
  code: string;
}
