import {BrandRespose} from './../response/brand.response';

export class BrandRequest implements BrandRespose {
  description: string;
  id: number;
  name: string;
  slug: string;
}
