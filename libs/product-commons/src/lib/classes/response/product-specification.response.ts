export class ProductSpecificationResponse {
  id: number;
  title: string;
  description: string;
}
