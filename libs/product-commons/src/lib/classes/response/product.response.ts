import {BrandRespose} from '@product/classes/response/brand.response';
import {CategoryRespose} from '@product/classes/response/category.response';
import {ProductSpecificationResponse} from '@product/classes/response/product-specification.response';

export class ProductRespose {
  id: number;
  name: string;
  description: string;
  price: number;
  code: string;
  image: string;
  imageDetail: string;
  brand: BrandRespose;
  category: CategoryRespose;
  specifications: ProductSpecificationResponse[];
}
