export class ApiResponse<T> {
  status: boolean;
  code?: number;
  message?: string;
  total?: number;
  page?: number;
  pageItems?: number;
  data?: T;
}
