export class UploadResponse {
  filename: string;
  domain: string;
  url: string;
}
