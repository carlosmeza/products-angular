import {UploadResponse} from '@product/classes/response/upload.response';

export class UploadModel {
  domain: string;
  filename: string;
  url: string;

  constructor(data?: UploadResponse) {
    this.domain = data && data.domain || null;
    this.filename = data && data.filename || null;
    this.url = data && data.url || null;
  }
}
