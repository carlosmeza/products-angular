import {TokenRespose} from './../response/token.response';

export class TokenModel {
  expireAt: string;
  token: string;

  constructor(data?: TokenRespose) {
    this.expireAt = data && data.expireAt || null;
    this.token = data && data.token || null;
  }
}
