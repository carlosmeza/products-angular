import {CategoryRespose} from '@product/classes/response/category.response';

export class CategoryModel {
  id: number;
  name: string;
  slug: string;
  image: string;

  constructor(data?: CategoryRespose) {
    this.id = data && data.id || null;
    this.name = data && data.name || null;
    this.image = data && data.image || null;
    this.slug = data && data.slug || null;
  }
}
