import {ProductRespose} from '@product/classes/response/product.response';
import {BrandModel} from '@product/classes/model/brand.model';
import {CategoryModel} from '@product/classes/model/category.model';
import {ProductSpecificationModel} from '@product/classes/model/product-specification.model';

export class ProductModel {
  id: number;
  brand: BrandModel;
  category: CategoryModel;
  specifications: ProductSpecificationModel[];
  name: string;
  description: string;
  code: string;
  price: number;
  image: string;
  imageDetail: string;

  constructor(data?: ProductRespose) {
    this.id = data && data.id || null;
    this.brand = data && new BrandModel(data.brand) || null;
    this.category = data && new CategoryModel(data.category) || null;
    this.specifications = data && new ProductSpecificationModel().parseToModel(data.specifications) || null;
    this.name = data && data.name || null;
    this.description = data && data.description || null;
    this.code = data && data.code || null;
    this.price = data && data.price || null;
    this.image = data && data.image || null;
    this.imageDetail = data && data.imageDetail || null;
  }

}
