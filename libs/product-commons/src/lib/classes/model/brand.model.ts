import {BrandRespose} from './../response/brand.response';

export class BrandModel {
  description: string;
  id: number;
  name: string;
  slug: string;

  constructor(data?: BrandRespose) {
    this.id = data && data.id || null;
    this.name = data && data.name || null;
    this.description = data && data.description || null;
    this.slug = data && data.slug || null;
  }
}
