export interface UserInterface {
  id: number;
  username: string;
  role: UserRol;
}

export class UserModel {
  id: number;
  username: string;
  role: UserRol;

  constructor(data: UserInterface) {
    this.id = data.id || null;
    this.username = data.username || null;
    this.role = data.role || null;
  }

  get isAdmin(): boolean {
    return this.role === UserRol.ADMIN;
  }

  get isStudent(): boolean {
    return this.role === UserRol.USER;
  }

  isAuthorized(rols: UserRol[]) {
    return rols.includes(this.role);
  }
}

export enum UserRol {
  ADMIN = 'ROLE_ADMIN',
  USER = 'ROLE_USER'
}
