import {ProductSpecificationResponse} from '@product/classes/response/product-specification.response';

export class ProductSpecificationModel {
  id: number;
  title: string;
  description: string;

  constructor(data?: ProductSpecificationResponse) {
    this.id = data && data.id || null;
    this.title = data && data.title || null;
    this.description = data && data.description || null;
  }

  parseToModel(specifications: ProductSpecificationResponse[]): ProductSpecificationModel[] {
    if (specifications) {
      return specifications.map(s => new ProductSpecificationModel(s));
    }
    return [];
  }
}
