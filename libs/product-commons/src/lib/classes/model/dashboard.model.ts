import {DashboardResponse} from '@product/classes/response/dashboard.response';

export class DashboardModel {
  brands: number;
  categories: number;
  products: number;

  constructor(data?: DashboardResponse) {
    this.brands = data && data.brands || 0;
    this.categories = data && data.categories || 0;
    this.products = data && data.products || 0;
  }
}
