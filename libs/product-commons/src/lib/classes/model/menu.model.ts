export class MenuModel {
  icon: string;
  title: string;
  path: string[];
}
