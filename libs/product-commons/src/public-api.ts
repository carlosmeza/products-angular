/*
 * Public API Surface of product-commons
 */

export * from './lib/product-commons.module';
export * from './lib/classes/classes.module';
export * from './lib/guards/admin-guards.module';
